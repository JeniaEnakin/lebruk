// .___  ___.  __  .______      .______        ______           _______.___________. __    __   _______   __    ______
// |   \/   | |  | |   _  \     |   _  \      /  __  \         /       |           ||  |  |  | |       \ |  |  /  __  \
// |  \  /  | |  | |  |_)  |    |  |_)  |    |  |  |  |       |   (----`---|  |----`|  |  |  | |  .--.  ||  | |  |  |  |
// |  |\/|  | |  | |      /     |      /     |  |  |  |        \   \       |  |     |  |  |  | |  |  |  ||  | |  |  |  |
// |  |  |  | |  | |  |\  \----.|  |\  \----.|  `--'  |    .----)   |      |  |     |  `--'  | |  '--'  ||  | |  `--'  |
// |__|  |__| |__| | _| `._____|| _| `._____| \______/     |_______/       |__|      \______/  |_______/ |__|  \______/


'use strict';

$(function () {


	var _setListeners = function () {

	};

	var $mainSlider = {
		slider: $('#main-slider'),
		nav: $('#main-slider-nav')
	};


	var init = function () {
		_setListeners();

		$("#clientsCarousel").slick({
		  arrows: true,
		  dots: false,
		  slidesToShow: 4,
		  focusOnSelect: true,
			slide: '.item',
		  infinite: true
		});
		$mainSlider.slider.slick({
			dots: false,
			arrows: false,
			slide: '.item',
			slidesToShow: 1,
			fade: true,
			asNavFor: $mainSlider.nav
		});
		$mainSlider.nav.slick({
			dots: false,
			arrows: false,
			slide: '.item',
			focusOnSelect: true,
			slidesToShow: 1,
			asNavFor: $mainSlider.slider
		});
		var $blurArr = [
			{
				id : "#some-element-1",
				href: "images/works/img-1.jpg"
			},
			{
				id : "#some-element-2",
				href: "images/works/img-2.jpg"
			},
			{
				id : "#some-element-3",
				href: "images/works/img-3.jpg"
			},
			{
				id : "#some-element-4",
				href: "images/works/img-4.jpg"
			},
			{
				id : "#some-element-5",
				href: "images/works/img-5.jpg"
			},
			{
				id : "#some-element-6",
				href: "images/works/img-6.jpg"
			},
			{
				id : "#some-element-7",
				href: "images/works/img-7.jpg"
			},
			{
				id : "#some-element-8",
				href: "images/works/img-8.jpg"
			},
			{
				id : "#some-element-9",
				href: "images/works/img-9.jpg"
			}
		];
		var blurConfig = {
			blurAmount: 4
		};
		for (var i=1; i <= $blurArr.length; i++) {
			$('#some-element-'+ i).backgroundBlur({
				imageURL : "images/works/img-"+i+".jpg",
				blurAmount : blurConfig.blurAmount,
				imageClass : 'bg-blur'
			});
		}
	};
	init();

	// $('body').pixelPerfect({
	//     path: 'main.jpg', //default mockup path
	//     draggable: false, //draggable state
	// });

});