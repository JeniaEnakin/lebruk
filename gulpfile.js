'use strict';


var gulp = require('gulp'),
    minifyCss  = require('gulp-minify-css'),
    rename     = require('gulp-rename'),
    concat     = require('gulp-concat'),
    uglify     = require('gulp-uglify'),
    autopref   = require('gulp-autoprefixer'),
    concatCss  = require('gulp-concat-css'),
    jade       = require('gulp-jade'),
    sass       = require('gulp-sass');

var browserSync = require('browser-sync').create(),
  reload      = browserSync.reload;


var config = {
    name : 'lebruk',
    builtDirCss : 'assets/build/stylesheets/',
    builtDirJs : 'assets/build/js/'

};

gulp.task('sass', function () {

    return gulp.src([
        'assets/sass/constructor.scss',
        'assets/sass/**/*.scss',
        'assets/sass/**/*.sass'
    ])
        .pipe(sass())
        .pipe(autopref('last 10 versions'))
        .pipe(gulp.dest('assets/build/stylesheets'))
});
gulp.task('concat', function () {
    return gulp.src([
        'assets/build/stylesheets/constructor.css'
    ])
        .pipe(concatCss(
            config.name + '.min.css'
        ))
        // .pipe(minifyCss())
        .pipe(gulp.dest(config.builtDirCss))
});


gulp.task('js_lib', function() {
    return gulp.src([
        // 'bower_components/moment/moment.js',
        // 'assets/js/jQuery.filer-master/js/jquery.filer.js',
        // 'assets/js/jQuery.filer-master/js/custom.js',
        'bower_components/bootstrap/js/transition.js',
        'bower_components/bootstrap/js/modal.js',
        'bower_components/background-blur/dist/background-blur.min.js',
        // 'bower_components/bootstrap/js/collapse.js',
        // 'bower_components/bootstrap/js/dropdown.js',
        // 'assets/js/jquery.datepick.package-5.0.0/jquery.plugin.js',

        // 'assets/js/jquery.datepick.package-5.0.0/jquery.datepick.js',
        'assets/js/slick.js',
        
        // 'assets/js/bps.js'
        ]
    )
        
        .pipe(concat(config.name + '.lib.js'))
        .pipe(gulp.dest(config.builtDirJs))
        .pipe(rename({ suffix: '.min' }))
        // .pipe(uglify())
        // .pipe(livereload(server))
        .pipe(gulp.dest(config.builtDirJs));
});
gulp.task('scripts', function() {
    return gulp.src([
        //'assets/js/classie.js',
        //'assets/js/uisearch.js',
        //'assets/js/contact.maps.js',
        'assets/js/config.js'
    ])
        .pipe(concat('config.js'))
        .pipe(rename({ suffix: '.min' }))
        // .pipe(uglify())
        .pipe(gulp.dest(config.builtDirJs));
});
gulp.task('serve', [], function() {

    browserSync.init({
        server: "./assets/build"
    });

    // gulp.watch("assets/build/*.html").on('change', browserSync.reload);
});
gulp.task('watch', function () {
 

    gulp.watch([
        'assets/sass/constructor.scss',
        'assets/sass/**/*.scss',
        'assets/sass/**/*.sass'], ['sass']);
    gulp.watch(
        'assets/build/stylesheets/constructor.css', ['concat']);
    gulp.watch(
        ['assets/jade_view/*.jade','assets/jade_view/layout/*.jade'], ['jade']);
    gulp.watch('assets/**/*.js', ['scripts']);
}); 

gulp.task('jade', function() {
    return gulp.src('assets/jade_view/*.jade')
        .pipe(jade({pretty: true}))
        .pipe(gulp.dest('assets/build')); // tell gulp our output folder
});

gulp.task('default', ['serve','scripts', 'js_lib', 'sass', 'jade', 'concat', 'watch']);